# json state machine documentation

data types | description
---------- | -----------
states | Object array which includes all states. Every state has an integer id (id), a startstate integer bool (startstate), a description string (desc), an onEntry string (onEntry), a transitions array (transitions), and an onExit string (onExit).
id | Integer unique id for a state.
startstate | Integer boolean. If set to > 0 it's true.
desc | Description string for describing the states purpose.
onEntry | String for pasting C++ code into the state which will be executed when the state is being entered. Can be null.
transitions | Array which includes integer state pointer to the transitions target (to) and a condition (cond) which has to be true to change the state to the given to integer state pointer. Can be an empty array.
onExit | String for pasting C++ code into the state which will be executed when the state is left. Can be null.
to | Integer state pointer inside of transitions array to point to the state id to which the transition should change.
cond | String inside of transitions array whichs holds the condition for the to integer state pointer to change the state.
includes | String array with includes that the generated C++ code should inherit.

The sequence of the json objects must not change.

Example json state machine:

```json
{
  "states": [
    {
      "id": 1,
      "startstate": 1,
      "desc": "this is a test state",
      "onEntry": null,
      "transitions": [
        {
          "to": 2,
          "cond": "1 == 1"
        },
        {
          "to": 1,
          "cond": "0"
        }
      ],
      "onExit": "printf(\"state id=1 left\");"
    },
    {
      "id": 2,
      "startstate": 0,
      "desc": "this is test state two",
      "onEntry": "printf(\"state id=2 entered\");",
      "transitions": [],
      "onExit": null
    }
  ],
  "includes": ["#include <cstdlib>", "#include <string>", "#include <iostream>"]
}

   
