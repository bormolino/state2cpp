# state2cpp

Generates C++ code from a json based state machine format.

### Requirements:
*  build-essential
*  libc6-dev
*  make
*  gcc-8
*  gcc-8-aarch64-linux-gnu
*  gcc-mingw-w64-x86-64


### Build source:

`make all`


### Clean build environment:

`make clean`


### Documentation:

Click [here](./doc/html/index.html)


### json state machine documentation

Click [here](./json-state-machine.md)


## Authors
* **Maximilian Borm** - *Developer* - [@bormolino](https://gitlab.com/bormolino)
* **Lars Kaufmann** - *Developer* - [@BartiWorscht](https://gitlab.com/BartiWorscht)
