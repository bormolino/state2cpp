/*
 * config.h: main configuration file
 *
 * Include this file before all other header files!
 *
 *
 * Version 1.1-stable
 * Author: Maximilian Borm
 *
 * All rights reserved
 *
 *
 * state2cpp is distributed under the GNU GENERAL PUBLIC LICENSE (GPL)
 * Version 2 (June 1991). See the "LICENSE" file distributed with this
 * software for more info.
 */


#ifndef __CONFIG_H
# define __CONFIG_H

#ifndef DEBUG
# define DEBUG    1 /* 0: none, 1: debug */
#endif

#if __STRICT_ANSI__ && ! (__STDC_VERSION__ >= 199900L)
# warning "Your standard C90 is to old! Please compile with at least C99 or GNU C89"
#endif

#ifdef __WINNT__
# define __USE_MINGW_ANSI_STDIO   1 /* enable standard C in stdio.h */
#endif

#if __GNUC__ >= 7
# pragma GCC diagnostic ignored "-Wimplicit-fallthrough"	/* allow switch-case cascades */
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define JSMN_STRICT /* activate strict mode for valid json */
#include "jsmn.h"

#include "func.h"

#define MAX_TOKENS 1024 /* max. tokens for jsmn - default 1024 */
#define JSON_FILE "json/demo.json" /* name of state-machine file */
#define CPP_FILE  "main.gen.cpp"   /* name of generated cpp file */
#define MAX_TRANS 32  /* max. numbers of transitions parsed per state - default 32 */

#endif /* __CONFIG_H */
