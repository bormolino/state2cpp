/*
 * func.c: functions for state2cpp.
 *
 * Version 1.1-stable
 * Author: Maximilian Borm
 *
 * All rights reserved
 *
 *
 * state2cpp is distributed under the GNU GENERAL PUBLIC LICENSE (GPL)
 * Version 2 (June 1991). See the "LICENSE" file distributed with this
 * software for more info.
 */

#ifndef __FUNC_H
# define __FUNC_H

/**
  * jsoneq function to check string-equals of json parsed jsmn-structures
  *
  * @param json const char pointer to json array
  * @param tok jsmntok_t pointer to jsmn token of parsed json to be checked with s
  * @param s const char pointer to json object name to be checked with s
  */
inline extern int jsoneq(const char *json, jsmntok_t *tok, const char *s) {
  return (tok->type == JSMN_STRING && (int) strlen(s) == tok->end - tok->start && strncmp(json + tok->start, s, tok->end - tok->start) == 0);
}

/**
  * rembcksl function is used to remove backslashes. jsmn can detect escaped chars in json but does not remove it in the output so we remove it with this function
  *
  * @param s char array with escaped chars
  */
inline extern void rembcksl(char s[]) {
  int c = 0;

  for (int i = 0; s[i] != '\0'; ++i)
    if(s[i] != '\\')
      s[c++] = s[i];

  s[c] = '\0';
}

#endif /* __FUNC_H */
