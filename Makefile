#
# Makefile
#
# Version 1.1-stable
# Author: Maximilian Borm

CC = gcc-8
CFLAGS = -std=c11
# optimizer flags
OFLAGS = -O3 -fomit-frame-pointer
PFLAGS =
# warning flags
WFLAGS = -Wall -Wextra -Werror

#disable for debugging
STRIP = -s

MARCH = -march=native -mtune=native
LFLAGS = $(RELRO) $(STRIP)

NPROC != nproc

ifeq ($(target), win)
CC = x86_64-w64-mingw32-gcc-posix
EXT = .exe
else
RELRO = -Wl,-z,relro,-z,now
endif

ifeq ($(target), arm)
CC = aarch64-linux-gnu-gcc-8
EXT = -arm
MARCH = -march=armv8-a -mtune=generic
endif


# HEADER =
EXE1 = main$(EXT)
OBJ1 = main.c


all:
	@clear
	$(MAKE) clean
	mkdir -pv bin
	$(MAKE) V=1 -j $(NPROC) $(EXE1)
	$(MAKE) V=1 target=win -j $(NPROC) $(EXE1).exe
	$(MAKE) V=1 target=arm -j $(NPROC) $(EXE1)-arm

$(EXE1): $(OBJ1)
	$(CC) $(MARCH) $(WFLAGS) $(PFLAGS) $(LFLAGS) $(CFLAGS) $(OFLAGS) $+ -o bin/$@


.PHONY: clean
clean:
	rm -vf bin/* > /dev/null 2>&1 ; true
	rmdir -pv bin > /dev/null 2>&1 ; true
